module gitlab.com/bLuka/blog

go 1.21.0

require (
	github.com/canstand/compost v0.6.4 // indirect
	github.com/davidhampgonsalves/hugo-black-and-light-theme v0.0.0-20231209011516-e024de785b91 // indirect
	github.com/nodejh/hugo-theme-mini v0.0.0-20240322021354-34417888405c // indirect
	github.com/rhazdon/hugo-theme-hello-friend-ng v0.0.0-20231122225555-51e697bea7eb // indirect
)
