---
title: "whoami"
date: "2022-12-24T11:25:31+01:00"
author: "Luka"
authorTwitter: "" # do not include @
cover: ""
tags:
  - "whoami"
keywords:
  - "whoami"
description: "Who am I"
showFullContent: true
readingTime:
hideComments: false
color: "" # color from the theme settings
---

Software developer by masochism since 2010, and 2017 professionaly, I have a strong passion around system architecture
and automation.

DevOps during the day, [r/DataHoarder](https://www.reddit.com/r/DataHoarder/) during the night, this blog aims to keep a
summary of my journey into exotic setups or architectures for my future self.

Eventually, and thanks to the urge to justify myself, I might also shamelessly use it as a factorized answer to the
question “But what have you done?!”.

Craving to share your contempt? Find me on Mastodon: [@luka@bsd.network](https://bsd.network/@luka)
