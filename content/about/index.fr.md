---
title: "whoami"
date: "2022-12-24T11:25:31+01:00"
author: "Luka"
cover: ""
tags:
  - "whoami"
keywords:
  - "whoami"
description: ""
showFullContent: true
readingTime: false
hideComments: false
color: ""
---

Développeur logiciel par masochisme depuis 2010, et professionnellement depuis 2017, je me passionne pour l’architecture
logicielle et l’automatisation.

DevOps le jour, [r/DataHoarder](https://www.reddit.com/r/DataHoarder/) la nuit, ce blog vise à conserver un résumé de
mon périple au travers de configurations ou d’architectures exotiques pour mon moi futur.

Éventuellement, et face au besoin urgent de me justifier, je me permettrais certainement de l’utiliser sans honte comme
réponse factorisée à la question « Mais qu’as-tu fait ?! ».

Une envie irrésistible de partager votre dédain ? Retrouvez-moi sur Mastodon :
[@luka@bsd.network](https://bsd.network/@luka)
