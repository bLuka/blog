---
title: "[ZFS] Special vdev tweaks"
date: 2024-05-03T23:48:39+02:00
draft: true
---

## Contexte de mise en place du special vdev


## Problèmes de performance

Lors de mes investigations de dégradation de disque, je me suis rendu compte que les deux NVMe en miroir avaient des
performancess *très* différentes. Pour cause, une erreur d'achat (j'ai confondu un Western Digital Black SN850 et un
Samsung 980 Pro).

Les specs papier et les benchmarks étant pourtant très proches, j'ai découvert la joie des schedulers firmwares… Pour
une raison qui m'échappe, le scheduler du Samsung 980 Pro passe parfois à un « mode » particulier I/O où les lectures
sur le SSD passent à 8ms (très stables) de latence, contre ~0.3ms à chaud.

Ça ressemble à un mode de veille/basse consommation, du contrôleur. Sauf que ce comportement étant différent de son
jumeau en miroir, le WD Black SN850, le scheduler ZFS évitait d'allouer des requêtes de lectures au Samsung, privilgiant
le WD. Problème : le NVMe Samsung restait dans son mode haute latence, faute d'être sollicité.

Mes investigations de ralentissements I/O semblaient pointer vers une sur-utilisation du WD, avec des pics réguliers à
100% util.

## Remplacement du disque

J'ai fini par racheter un WD SN850 et remplacer le Samsung (finalement peu utilisé), qui est parti pour quelqu'un qui en
aura certainement un meilleur usage.

Après le remplacement hardware, petit check-up, avec un problème inattendu :

```
# zpool status -v

  pool: goliath
 state: DEGRADED
status: One or more devices is currently being resilvered.  The pool will
	continue to function, possibly in a degraded state.
action: Wait for the resilver to complete.
  scan: resilver in progress since Sat May  4 00:05:17 2024
	2.92T / 42.0T scanned at 25.3G/s, 7.85G / 39.1T issued at 68.1M/s
	0B resilvered, 0.02% done, no estimated completion time
remove: Removal of vdev 6 copied 620M in 0h0m, completed on Sun Sep 10 00:03:23 2023
	11.7K memory used for removed device mappings
config:

	NAME                                                    STATE     READ WRITE CKSUM
	goliath                                                 DEGRADED     0     0     0
	  mirror-0                                              DEGRADED     0     0     0
	    ata-ST12000NE0008-1ZF101_ZTN0Z5H4-part1             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR50GFXE-part2             FAULTED      6     3     0  too many errors
	  mirror-4                                              ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504ES7-part2             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504PA2-part2             ONLINE       0     0     0
	  mirror-5                                              ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504EGE-part2             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR5049G6-part2             ONLINE       0     0     0
	special
	  mirror-7                                              DEGRADED     0     0     0
	    nvme-WDS100T1X0E-00AFY0_2140JY444704-part1          ONLINE       0     0     0
	    12381564402743728580                                UNAVAIL      0     0     0  was /dev/disk/by-id/nvme-Samsung_SSD_980_PRO_1TB_S5GXNX0W378949K-part1

errors: No known data errors
```

Le Samsung apparaît bien comme `UNAVAIL`, comme attendu (j'aurais préféré un remplacement à chaud sans avoir eu besoin
de déconnecter le Samsung, histoire d'éviter d'être dans une situation de SPOF, même temporaire).

Ceci dit, autre souci : un des disques du stockage principal, `ST18000NM000J-2TV103_WR50GFXE`, remonte des erreurs de
lecture et d'écriture. Peut-être un câble mal connecté ?

En attendant, je le déconnecte (`zpool offline goliath [ID du disque]`), et je lance le remplacement du NVMe.

### 0. Vérifier et tout revérifier plusieurs fois

C'est une petite astuce qui vient avec l'expérience d'avoir cassé et perdu des tébioctets de données, mais ça vaut la
peine de le répéter (au moins pour mon futur moi qui me lira) : _toujours_ relire plusieurs fois, à tête reposer, toutes
les opérations qui peuvent être destructrices.

- **Vérifier ses backups off-site** et identifier les données qu'on risque de perdre en cas de pépin, et celles qu'il
  faudra restaurer
- Vérifier qu'on modifie bien le bon disque (l'utilisation de `/dev/disk/by-id` devrait figurer dans les conventions
  internationnales)
- Vérifier les données qu'il contient avant toute modification
- Identifier toutes les commandes qui peuvent être destructrices **avant** de les taper
- Prendre trente secondes pour respirer et bien relire avant d'approcher les mains du bouton entrée
- Idéalement, faire relire par un copain
- Préférer faire ce genre de manip' quand on est bien en forme et pas trop fatigué

### 1. Partitionner le nouveau disque

Petite vérification rapide du formattage de l'ancien disque, et préparation du nouveau :

```
# fdisk -l /dev/disk/by-id/nvme-WDS100T1X0E-00AFY0_2140JY444704

Disque /dev/disk/by-id/nvme-WDS100T1X0E-00AFY0_2140JY444704 : 931,51 GiB, 1000204886016 octets, 1953525168 secteurs
Modèle de disque : WDS100T1X0E-00AFY0
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : gpt
Identifiant de disque : 3E39429C-2641-43FF-90AF-071BBAD5E363

Périphérique                                                    Début        Fin   Secteurs Taille Type
/dev/disk/by-id/nvme-WDS100T1X0E-00AFY0_2140JY444704-part1       2048 1711290367 1711288320   816G Système de fichiers Linux
/dev/disk/by-id/nvme-WDS100T1X0E-00AFY0_2140JY444704-part2 1711290368 1767972863   56682496    27G Système de fichiers Linux
```

On relève juste les numros de secteurs pour reporter sur le disque suivant :

```
# parted /dev/disk/by-id/nvme-WD_BLACK_SN850X_1000GB_24127V4A3114

GNU Parted 3.4
Utilisation de /dev/nvme0n1
Bienvenue sur GNU Parted ! Tapez « help » pour voir la liste des commandes.
(parted) mktable gpt
(parted) mkpart primary ext2 2048s 1711290367s
(parted) print
Modèle : WD_BLACK SN850X 1000GB (nvme)
Disque /dev/nvme0n1 : 1000GB
Taille des secteurs (logiques/physiques) : 512B/512B
Table de partitions : gpt
Drapeaux de disque :

Numéro  Début   Fin    Taille  Système de fichiers  Nom   Drapeaux
 1      1049kB  876GB  876GB
```

(À noter que `fdisk -l` retourne les emplacements de partition en secteurs, et que `parted` supporte l'utilisation des
numéros de secteurs avec le suffix `s` lors de la création de la partition.)

### 2. Remplacement du disque sur ZFS

On-ne-peut plus simple ! `zpool-replace(8)` est hyper simple d'usage :

```
zpool replace goliath [ancien ID du disque] /dev/disk/by-id/[nouveau disque]
```

### 3. Surveillance du resilver

Le remplacement devrait créer un vdev `replacing-1` avec l'ancien périphérique, et le nouveau.

`zpool-status(8)` devrait indiquer des statistiques de resilver (vitesse de scan, vitesse de traitement, pourcentage
traité, et estimation du temps restant).

```
# zpool status -v

  pool: goliath
 state: DEGRADED
status: One or more devices is currently being resilvered.  The pool will
	continue to function, possibly in a degraded state.
action: Wait for the resilver to complete.
  scan: resilver in progress since Sat May  4 00:05:17 2024
	18.7T / 42.0T scanned at 470M/s, 4.01T / 27.3T issued at 101M/s
	10.6G resilvered, 14.67% done, 2 days 19:26:54 to go
remove: Removal of vdev 6 copied 620M in 0h0m, completed on Sun Sep 10 00:03:23 2023
	11.7K memory used for removed device mappings
config:

	NAME                                                    STATE     READ WRITE CKSUM
	goliath                                                 DEGRADED     0     0     0
	  mirror-0                                              DEGRADED     0     0     0
	    ata-ST12000NE0008-1ZF101_ZTN0Z5H4-part1             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR50GFXE-part2             OFFLINE      3     0     0
	  mirror-4                                              ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504ES7-part2             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504PA2-part2             ONLINE       0     0     0
	  mirror-5                                              ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504EGE-part2             ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR5049G6-part2             ONLINE       0     0     0
	special
	  mirror-7                                              DEGRADED     0     0     0
	    nvme-WDS100T1X0E-00AFY0_2140JY444704-part1          ONLINE       0     0     0
	    replacing-1                                         DEGRADED     0     0     0
	      12381564402743728580                              UNAVAIL      0     0     0  was /dev/disk/by-id/nvme-Samsung_SSD_980_PRO_1TB_S5GXNX0W378949K-part1
	      nvme-WD_BLACK_SN850X_1000GB_24127V4A3114_1-part1  ONLINE       0     0     0  (resilvering)

errors: No known data errors
```

**Pro-tip :** d'expérience, pour les disques durs, les vitesses de scan et de traitement s'effondrent sur les derniers
90%, avec parfois des performances 5 à 10 fois moindres. L'estimation du temps restant est donc évidemment à prendre
avec des pincettes, et n'est évidemment pas à considérer à la lettre.


### 4. Fin du resilver

Le resilver s'est fini assez vite :
- le scan (lecture) de toute la pool était nécessaire avant de finir le resilver
- seul le traitement des données du NVMe (écriture) était nécessaire, donc pas de bottleneck à l'écriture sur les
  disques durs

```
# zpool status -v

  pool: goliath
 state: DEGRADED
status: One or more devices has experienced an unrecoverable error.  An
	attempt was made to correct the error.  Applications are unaffected.
action: Determine if the device needs to be replaced, and clear the errors
	using 'zpool clear' or replace the device with 'zpool replace'.
   see: https://openzfs.github.io/openzfs-docs/msg/ZFS-8000-9P
  scan: resilvered 614G in 1 days 03:34:41 with 0 errors on Sun May  5 03:39:58 2024
remove: Removal of vdev 6 copied 620M in 0h0m, completed on Sun Sep 10 00:03:23 2023
	11.7K memory used for removed device mappings
config:

	NAME                                                  STATE     READ WRITE CKSUM
	goliath                                               DEGRADED     0     0     0
	  mirror-0                                            DEGRADED     0     0     0
	    ata-ST12000NE0008-1ZF101_ZTN0Z5H4-part1           ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR50GFXE-part2           OFFLINE     10    81     0
	  mirror-4                                            ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504ES7-part2           ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504PA2-part2           ONLINE       0     0     0
	  mirror-5                                            ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR504EGE-part2           ONLINE       0     0     0
	    ata-ST18000NM000J-2TV103_WR5049G6-part2           ONLINE       0     0     0
	special
	  mirror-7                                            ONLINE       0     0     0
	    nvme-WDS100T1X0E-00AFY0_2140JY444704-part1        ONLINE       0     0     0
	    nvme-WD_BLACK_SN850X_1000GB_24127V4A3114_1-part1  ONLINE       0     0     0

errors: No known data errors
```

On peut noter que la pool est toujours flaggée `DEGRADED` tant que le mirroir et le disque dur défaillant n'est pas
remplacé.

Comme j'ai acheté le disque dur sur Amazon il y a 6 mois, je profite de la plateforme pour la demande de remplacement.
Le remplacement du disque dur devrait se dérouler comme le remplacement du special vdev.

## Nouvelles performances
