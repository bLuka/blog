---
title: "Downsampling InfluxDB pour un historique long"
date: 2024-05-05T17:28:30+02:00
draft: true
---

Cet article suit mes avancées du [monitoring de mon homelab avec Grafana &
InfluxDB](./2024-04-26_sysmon_with_grafana.fr.md).

Une première limitation, à l'usage, d'InfluxDB, sont ses performances faibles pour des requêtes sur de gros volumes de
données. C'est une problématique que je rencontre avec Grafana, pour les fenêtres temporelles de plus de 2h : malgré une
limite de nombre de points inférieure à 50 points, l'aggrégation et le calcul de la moyenne/min/max sont coûteux, et
saturent les ressources CPU (pourtant 2c/4t) de mon NUC, pendant plusieurs très longues minutes _par requête_.

## Axes d'amélioration
