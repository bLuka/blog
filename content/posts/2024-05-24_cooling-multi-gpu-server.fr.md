---
title: "Refroidir un serveur multi-GPU : Choix du matériel"
date: 2024-05-24T11:59:18+02:00
draft: false
toc: true
tags:
  - hardware
  - watercooling
---

Cela fait quelques semaines que j’ai identifié des problèmes de refroidissement sur Gath, mon serveur multi-usages et
multi-GPU ([présenté plus en détail ici](../2024-04-25_homelab/#gath-serveur-multi-usages)) :

![Photo intérieure du châssis noir de Gath, montrant un système de refroidissement liquide, trois cartes graphiques et 6
disques durs montés en façade. Des ventilateurs sont disposés en bas, en haut, et à l’arrière du
boîtier.](/imgs/homelab/gath-hardware.png "Photo de l’intérieur de Gath, pour illustrer la problématique d’air-flow")

À l’usage, surprise ! Contre toute attente et toute mise en garde, l’empilement des cartes graphiques ne fonctionne pas
très bien pour le refroidissement :

- AMD RX 6950XT idle : 65°C
- RTX 3080 FE idle : 34°C

Le radiateur de l’AMD RX 6950XT semble clairement sous-performer comparé à son équivalente Nvidia, mais ça n’est pas la
seule raison. L’air chaud expulsé par la RTX 3080 FE (par le haut), et l’espace très restreint sous la RX 6950XT, sont
probablement les deux principales causes des problèmes de refroidissement de cette carte.

Côté CPU, [j’ai aussi pu évoquer ses problèmes de refroidissement par ici](../2024-04-25_homelab/#le-refroidissement).

Depuis deux semaines, c’est la pompe de l’AIO de watercooling qui a lâché : en plus des problématiques de
refroidissement des cartes graphiques, le débit de liquide du watercooling du CPU est très largement insuffisant,
causant une surchauffe du processeur (>60°C dans le BIOS, >75°C _idle_ sous Linux) et une accélération de la vitesse des
ventilateurs (qui n’ont pourtant pas beaucoup de débit de liquide à refroidir).

Ce défaut de la pompe de l’AIO (un AIO Alphacool Eisbaer Pro Aurora 360) a causé plusieurs coupures du serveur par le
BIOS pour des dépassements de seuils critiques de température pendant des compilations de kernel Linux pour des mises à
jour (>100°C).

## Objectif

On va tenter de mettre en place une solution de refroidissement qui permette de faire d’une pierre deux coups : on
ajoute le GPU qui peine sur la boucle de watercooling existante (un AIO modulaire), et on rajoute une vraie pompe qui
fonctionne, sur le circuit de l’AIO actuel.

### Radiateur externe

Le radiateur 360mm interne qui s’occupe déjà du processeur est limité. Un threadripper consommant beaucoup, l’ajout
d’une carte graphique sur ce même radiateur sera probablement de trop.

Une règle simplifiée conseille 120mm de radiateur + ventilateur tous les 100W.

- Threadripper 3960X : 280W
- AMD RX 6950XT : 330W
- (*Si un jour…*) Nvidia RTX 3080 : 320W

Ça signifie qu’_au moins_ 6×120mm sont nécessaires pour refroidir l’ensemble des trois composants (s’ils sont sollicités
en même temps).

De manière logique, ajouter davantage de radiateur permet aussi de limiter le débit d’air nécessaire pour les mêmes
capacités de refroidissement.

Ça veut dire : plus de radiateur ➡️ moins besoin de ventilation ➡️ moins de bruit

Étant donné que l’intérieur du châssis est déjà bien occupé, je vise l’installation d’un radiateur externe. L’un des
meilleurs sur le marché et le plus fourni en accessoires me semble être côté des Mo-Ra. Je vise donc un Mo-Ra3 420 LT
(plus d’accessoires que les Mo-Ra3 360, et 4 ventilateurs de 200mm sont moins chers que 9 ventilateurs de 120mm).

### Design de la boucle de watercooling

L’objectif étant de construire une boucle en série avec le processeur, et l’AIO existant.

Pour savoir comment ordonner la boucle, il faut avoir à l’esprit que le premier périphérique dans la boucle chauffera le
liquide du second périphérique :

- le CPU tourne rarement à 100% (plutôt sur des recompilations/mises à jour système, pendant la nuit)
- le GPU est souvent sollicité en fin de journée pendant des périodes où le processeur est peu sollicité (<30%)

Comme le refroidissement du processeur n’est jamais critique en même temps que le refroidissement du GPU, on va passer
le GPU en premier dans la boucle :

```mermaid
flowchart LR
    GPU(GPU AMD)       --> CPU
    CPU(Processeur)    --> Rad
    Rad(Radiateur AIO) --> MoRa
    MoRa(Mo-Ra)        --> GPU
```

## (⏳ WiP) Liste de courses

Cette liste sera complétée au fur-et-à-mesure de mes recherches.

<table>
  <thead>
    <tr><th>Usage</th><th>Modèles</th><th>Description</th><th>Prix</th><th>Qté</th></tr>
  </thead>
  <tbody>
    <tr><td rowspan="2"><strong>Radiateur</strong></td><td>Mo-Ra3 LT 420</td><td>Un peu plus performant que le 360, et moins cher au global</td><td></td><td rowspan="2">1</td></tr>
    <tr><td><s>Mo-Ra3 LT 360</s></td><td>Moins performant, trop d’emplacements ventilateurs à remplir (donc plus cher au total)</td><td></td></tr>
    <tr><td><strong>Ventilateurs</strong></td><td>Noctua NF-A20</td><td>J’ai leurs petits frères, les NF-A12 et NF-A18, auxquels je n’ai rien à reprocher. Très silencieux à basse fréquence.</td><td></td><td>4</td></tr>
    <tr><td><strong>Module Mo-Ra</strong></td><td>Mo-Ra3 420 D5 Dual Pump</td><td>Pour pouvoir attacher le réservoir et les deux pompes au radiateur</td><td></td><td>1</td></tr>
    <tr><td><strong>Supports Mo-Ra</strong></td><td>Mo-Ra3 Wall/Case Mount</td><td>Pour pouvoir accrocher le radiateur au mur</td><td></td><td>1</td></tr>
    <tr><td><strong>Pompes</strong></td><td>À étudier</td><td>Deux pompes compatibles D5, pour assurer la redondance en cas de défaut de l’une d’elle (le temps du remplacement)</td><td></td><td>2</td></tr>
    <tr><td rowspan="2"><strong>QDC</strong><td>Alphacool</td><td>Compatible avec l’AIO actuel, mais ils sont apparemment très peu performants niveau débit/pression. Certains disent qu’ils ont tendance à couler s’ils sont déconnectés sans que le circuit soit purgé.</td><td></td><td rowspan="2">?</td></tr>
    <tr><td>Autres ?</td><td>L’objectif serait évidemment de pouvoir déconnecter le Mo-Ra ponctuellement s’il y a besoin de déplacer le serveur/le radiateur, sans avoir besoin de drainer la boucle</td><td></td></tr>
    <tr><td><strong>PCI bracket tube passthrough</strong></td><td>À étudier</td><td>Pas de trou dans le châssis, mais l’Enthoo 719 propose 4 slots PCIe verticaux disponibles pour sortir les tubes</td><td></td><td>1</td></tr>
    <tr><td><strong>PCI bracket Molex/PWM passthrough</strong></td><td>À étudier</td><td>Je ne vois pas de référence/de marque particulière qui en propose</td><td></td><td>1</td></tr>
    <tr><td><strong>Tubes</strong></td><td>À étudier</td><td>Des tubes souples qui permettent de déplacer le radiateur, de préférence</td><td></td><td>3m</td></tr>
    <tr><td rowspan="2"><strong>Rallonges Molex/PWM</strong></td><td rowspan="2">À étudier</td><td rowspan="2">Pour connecter l’alimentation et le contrôle PWM de la carte mère au bracket PCI d’une part, et connecter le bracket PCI au Mo-Ra d’autre part</td><td></td><td>20cm (châssis)</td></tr>
    <tr><td></td><td>1m (MoRa)</td></tr>
    <tr><td><strong>Fittings</strong></td><td>À étudier</td><td></td><td></td><td>?</td></tr>
    <tr><td><strong>GPU Waterblock</strong></td><td>À étudier</td><td>Un waterblock pour la Radeon RX 6950XT, idéalement compatible avec du métal liquide (déjà utilisé sur le CPU)</td><td></td><td>1</td></tr>
    <tr><td><strong>Liquide</strong></td><td>À étudier</td><td>Pas de préférence. Idéalement un liquide qui ne nécessite pas de maintenance/purge régulière.</td><td></td><td>1</td></tr>
    <tr><td><strong>Réservoir</strong></td><td>À étudier</td><td></td><td></td><td>1</td></tr>
    <tr><td><strong>Thermomètre</strong></td><td>À étudier</td><td>Probablement utile pour mieux mesurer des problèmes de refroidissement, j'ai vu que certaines pompes proposent un thermomètre + affichage intégré</td><td></td><td>1</td></tr>
    <tr><td><strong>Autres ?</strong></td><td></td><td>À compléter si oublis</td><td></td><td>1</td></tr>
  </tbody>
</table>
